# Landis+Gyr T550 Wärmezähler an vzlogger + volkszaehler

- kwhzmq.py liest per IR-Lesekopf wie [hier beschrieben](https://codeberg.org/HoSnoopy/T550-Ausleser/) aus und "sendet" es per ZMQ, es muss also immer im Hintergrund laufen.
- kwhlesen.py "empfängt" es per ZMQ
- kwhdelta.py "empfängt" den Zaehlerstand per ZMQ, liest einen Wert 5-60min (einstellbar, muß aber in der vzlogger.conf und dem kwhdelta.py gleich sein!) vorher aus und errechnet für diese Zeit die Leistung 

Führt man beim vzlogger direkt ein skript zum Auslesen aus, dauert die Antwort zu lange, also war das so meine Lösung. Das hat zusätzlich den Vorteil, daß man den Zählerstand, sofern man will, noch per cgi auslesen könnte.
Ich habe noch meine vzlogger.conf (auch mit Leistungsberechnung zusätzlich) dazugestellt. 

Im Volkszaehler-GUI habe ich dann folgenes für den neuen Kanal dafür ausgewählt:

- Titel: Nahwärme
- Typ: Sensor	
- Einheit: MWh
- Öffentlich: ja (ist aber egal)
- Farbe: #d32f2f (ist aber egal)
- Stil:	Linien (auch egal)
- Füllgrad: 0 
- Linienstil: Solide
- Achse: auto
- Auflösung: 1000 (Die Einheit ist in MWh, das script liefert jedoch KWh)

Die Default-Einstellung der Nachkommastellen ist in der ~/volkszaehler.org/htdocs/js/options.js unter "precision:" - diesen Wert  habe ich von 2 auf 3 hochgestellt, sonst wird es nicht KWh- sondern nur 10KWh-genau. 


