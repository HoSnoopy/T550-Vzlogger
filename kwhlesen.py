#!/usr/bin/python3

import time
import zmq

host="192.168.70.169"
thema="10000"
port=10000

context = zmq.Context()
sock = context.socket(zmq.SUB)
sock.connect("tcp://{}:{}".format(host, port))
sock.setsockopt_string(zmq.SUBSCRIBE, thema)

message = sock.recv().decode()
message = str(message)
kwh = message.rstrip().split(' ')[1]

print(kwh)

