#!/usr/bin/python3
#coding: utf-8

from __future__ import print_function
import subprocess
import serial, time
import zmq 

port = "10000"
host = "192.168.70.169"
thema = "10000"
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://{}:{}".format(host, port))


def lesen():
   ser = serial.Serial("/dev/ttyUSB0", baudrate=300, bytesize=7, parity="E", stopbits=1, timeout=2, xonxoff=0, rtscts=0)
   #send init message
   ser.write('\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'.encode())
   ser.write('\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'.encode())

   #send request message
   ser.write("/?!\x0D\x0A".encode())
   #ser.flush();
   ser.flushInput()
   ser.flushOutput()
   time.sleep(.5)

   #send read identification message
   vorlauf = (ser.readline())

   #change baudrate
   ser.baudrate=2400

   try:
     #read data message
     response=''
#     print(response.find('!'))
     while response.find('!')<0:
        response = ser.readline().decode('utf-8')
#        print(response)
        if response.find('6.8(') >= 0:
            response = response.rstrip().split('*')[0]
            response = response.rstrip().split('(')[1]
            response = float(response)
            response = int(response * 1000)
            resp = str(response)
#            print(response)
   except:
            response = 'error'
   ser.close()
#   print(resp)
#   time.sleep(5)
   return(resp)


while True:
     kwh = lesen()
     try:
       for n in range (50):
        socket.send_string("{} {}".format(thema, kwh))
#        print(thema + ' ' + kwh)
        time.sleep(.1)
     except KeyboardInterrupt:
        print('\ncaught keyboard interrupt!, bye')
        socket.close
        sys.exit()
     time.sleep(1)

