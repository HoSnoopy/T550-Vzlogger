#!/usr/bin/python3

import time
from datetime import datetime
import zmq
import json
import requests


host="192.168.70.169"
thema="10000"
port=10000

uuid="6c046270-56db-11ed-99ed-5708be7f976c"
middlewarehost="192.168.70.169"
interval = 30 #alle wieviel Minuten soll die Leistung berechnet werden? 



context = zmq.Context()
sock = context.socket(zmq.SUB)
sock.connect("tcp://{}:{}".format(host, port))
sock.setsockopt_string(zmq.SUBSCRIBE, thema)

message = sock.recv().decode()
message = str(message)
kwh = message.rstrip().split(' ')[1]


minuten = int(interval)
min = str(minuten)
url = 'http://'+host+'/middleware/data/'+uuid+'.json?from=' + min + ' minutes ago'

try:
    daten = requests.get(url)
    daten = daten.text
    daten = daten.rstrip().split('[[')[1]
    altstand = float(daten.rstrip().split(',')[1])
    altstand = altstand * 1000 #MWH -> KWH
    kwh = int(kwh)
    delta = kwh - altstand
    stunde = minuten/60 # interval in Stunden
    leistung = delta/stunde
    round(leistung, 3)
    print(leistung) #in Kilowatt
except: 
    print('0.000')

